﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hw260820
{
    class MyQueue
    {
        private List<Customer> customers;
        public void Enqueue(Customer c)
        {
            customers.Add(c);
            Console.WriteLine($"{c} is last in line");
        }

        internal void Add(Customer customer)
        {
            throw new NotImplementedException();
        }

        public Customer Dequeue()
        {
            Customer item = customers[0];
            customers.RemoveAt(0);
            return item;
        }
        public void Init(List<Customer> customers1)
        {
            customers = customers1;
        }
        public void Clear()
        {
            customers.Clear();
        }
        public Customer WhoIsNext()
        {
            Customer item = customers[0];
            return item;
        }
        public int Count
        {
            get;
        }
        public void SortByProtection()
        {
            QueueSortByProtection sortByProtection = new QueueSortByProtection();
            customers.Sort(sortByProtection);
        }
        public void SortByTotalPurchases()
        {
            QueueSortByTotalPurchases sortByTotalPurchases = new QueueSortByTotalPurchases();
            customers.Sort(sortByTotalPurchases);
        }
        public void SortByBirthYear()
        {
            QueueSortByBirthYear sortByBirthYear = new QueueSortByBirthYear();
            customers.Sort(sortByBirthYear);
        }
        public List<Customer> DequeueCustomers(int x)
        {
            customers.RemoveRange(0, x);
            customers = new List<Customer>(x);
            return customers;
        }
        public void AniRakSheela(Customer c)
        {
            customers.Insert(0, c);
            Console.WriteLine($"{customers[0]} Ani rak sheela");
        }
        public Customer DequeueProtectiza()
        {
            QueueSortByProtection sortByProtection = new QueueSortByProtection();
            customers.Sort(sortByProtection);
            Customer c = customers[0];
            customers.Remove(c);
            return c;
        }

    }
     class QueueSortByProtection : IComparer<Customer>
    {
        public int Compare(Customer x, Customer y)
        {
            return x.Protection - y.Protection;
        }
    }
    class QueueSortByTotalPurchases : IComparer<Customer>
    {
        public int Compare(Customer x, Customer y)
        {
            return x.TotalPurchases - y.TotalPurchases;
        }
    }
    class QueueSortByBirthYear : IComparer<Customer>
    {
        public int Compare(Customer x, Customer y)
        {
            return x.BirthYear - y.BirthYear;
        }
    }
}


