﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hw260820
{
    class Program
    {
        static void Main(string[] args)
        {
            List<Customer> customers = new List<Customer>();
            MyQueue myQueue = new MyQueue();
            customers.Add(new Customer(1, "Dani", 1998, "TLV", 50, 1500));
            customers.Add(new Customer(2, "Tzlil", 1996, "NYC", 60, 2841));
            customers.Add(new Customer(3, "Shimi", 1978, "LA", 23, 320));
            customers.Add(new Customer(4, "Dvir", 1983, "ATA", 34, 1400));
            customers.Add(new Customer(5, "Dor", 1993, "BER", 14, 542));
            myQueue.Init(customers);
            myQueue.SortByBirthYear();
            foreach (Customer m in customers)
            {
                Console.WriteLine(m);
            }
            Console.WriteLine("===============================");
            myQueue.SortByTotalPurchases();
            foreach (Customer m in customers)
            {
                Console.WriteLine(m);
            }
            Console.WriteLine("===============================");
            myQueue.WhoIsNext();
            myQueue.DequeueCustomers(2);
            foreach (Customer m in customers)
            {
                Console.WriteLine(m);
            }
            Console.WriteLine("=========================");
            myQueue.DequeueProtectiza();
            Console.WriteLine(customers);
        }
    }
}
